#!/bin/bash
fuseext2 -o rw+ ./floppy.img mnt &> /dev/null
echo "[Mounted]"
cp src/kernel mnt/kernel
echo "[Copied]"
fusermount -uz mnt
echo "[Unmounted]"
