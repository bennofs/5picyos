1. About
========
5picyOS is a an OS I develop in my spare time. It's based on a microkernel architecture
and will only provide support for running haskell programs (I still have to decide whether all servers are
written in Haskell). 

2. Todo
=======

  - Almost everything :D
  
3. Current status
=================

   - WIP: (Work in progress)
     - Text output (basic text output works, scrolling needs to be done)
   - Finished:
     - Booting and starting the execution of the kernel (with the help of GRUB)

4. Compiling
============

   - The code is only tested under linux (although it may be possible to compile it under other OSes, but the provided
      shell scripts wont work then)
   - To compile, build the image and run it in qemu, do the following on a command line in linux:
       1. change to the directory where the you put the src (cd Directory/of/source)
       2. type make -C src && ./update-image.sh && ./run-qemu.sh
       3. You may need to make the scripts ./update-image.sh and ./run-qemu.sh executable. 
   - Dependencies include standard build dependencies, and for the image, fuseext2. 
