#include "console.hpp"

namespace kernel {

IOPort<uint8_t> FramebufferDevice::m_control_register(0x3D4);
IOPort<uint8_t> FramebufferDevice::m_data_register(0x3D5);

void FramebufferDevice::writeChar(char c, uint8_t x, uint8_t y) {
	uint8_t color = (static_cast<uint8_t>(m_back_color) << 4) | static_cast<uint8_t>(m_current_color);
	(*this)(x,y) = static_cast<uint16_t>(color) << 8 | static_cast<uint16_t>(c);
}

void FramebufferDevice::setCursorPosition(uint8_t x, uint8_t y) {
	uint16_t cursor_location = y * 80 + x; // Screen is 80 chars wide
	m_control_register.set(14); // Write upper half
	m_data_register.set(static_cast<uint8_t>(cursor_location >> 8));
	m_control_register.set(15); // Write lower half
	m_data_register.set(static_cast<uint8_t>(cursor_location));
}


void Console::print(char const* string) {
	do {
		if(*string == '\n' || m_x > cpputil::get<1>(m_x_limits) )  {
			m_x = cpputil::get<0>(m_x_limits);
			++m_y;
			if(*string == '\n') continue;
		}
		m_device->writeChar(*string, m_x++, m_y);
	} while(*++string != '\0');
}

}