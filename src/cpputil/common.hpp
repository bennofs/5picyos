#pragma once

#include "variadic.hpp"

namespace kernel { namespace cpputil {

typedef unsigned int size_t;
typedef variadic<
	unsigned int,
	signed int,
	unsigned char,
	signed char,
	unsigned wchar_t,
	signed wchar_t,
	char16_t,
	char32_t,
	unsigned short,
	signed short,
	unsigned long,
	signed long,
	unsigned long long,
	signed long long
> integral_types;

typedef variadic<
	float,
	double,
	long double
> floating_point_types;	

}}