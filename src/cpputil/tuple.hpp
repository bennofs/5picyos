#pragma once

#include "type_traits.hpp"
#include "utility.hpp"

namespace kernel { namespace cpputil {

namespace detail {
	
	template <size_t ,  typename ... T>
	struct get_impl;
	
	template <typename H, typename ... T>
	struct get_impl<0, H, T...> {
		template <typename TT>
		static constexpr H& get(TT& t) { return t.m_data; }
	};
	
	template <size_t I, typename H, typename ... T>
	struct get_impl<I, H, T...> {
		template <typename TT>
		static constexpr typename at<I, H, T...>::type& get(TT& t) { return get_impl<I-1, T...>::get(t.m_tail); }
	};
	
}

template <typename ... Ts>
class tuple {};

template <typename T, typename ... R>
class tuple<T, R...> {
	
	public:
		
		tuple(T data, R... rest) : m_data(move(data)), m_tail(move(rest)...) {}
	
	private:
		T m_data;
		tuple<R...> m_tail;
		template <size_t I2, typename ... T2>
		friend class detail::get_impl;
};

template <size_t I, typename ... T>
typename at<I, T...>::type constexpr get(tuple<T...>& t) {
	return detail::get_impl<I, T...>::get(t);
}

template <size_t I, typename ... T>
typename at<I, T...>::type constexpr get(tuple<T...>const& t) {
	return detail::get_impl<I, const T...>::get(t);
}

template <typename First, typename Second>
using pair = tuple<First, Second>;



}}