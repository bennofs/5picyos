#pragma once

#include "common.hpp"

namespace kernel { namespace cpputil {

template <typename T, T V>
struct integral_constant {
	static constexpr T value = V;
	constexpr operator T() { return value; }
	typedef T value_type;
	typedef integral_constant type;
};

typedef integral_constant<bool, false> false_type;
typedef integral_constant<bool, true> true_type;
	
template <typename T> struct remove_reference      { typedef T type; };
template <typename T> struct remove_reference<T&>  { typedef T type; };
template <typename T> struct remove_reference<T&&> { typedef T type; };

template <typename T> struct remove_const          { typedef T type; };
template <typename T> struct remove_const<const T> { typedef T type; };

template <typename T> struct remove_volatile             { typedef T type; };
template <typename T> struct remove_volatile<volatile T> { typedef T type; };

template <typename T> struct remove_cv : remove_const<typename remove_volatile<T>::type> {};

template <bool, typename T = void> struct enable_if          {};
template <typename T>              struct enable_if<true, T> { typedef T type; };

template <bool, typename T1, typename> struct conditional               { typedef T1 type; };
template <typename T, typename T2>     struct conditional<false, T, T2> { typedef T2 type; };

template <size_t N, typename ... T>             struct at;
template <typename H, typename ... T>           struct at<0, H, T...> { typedef H type; };
template <size_t N, typename H, typename ... T> struct at<N, H, T...> : at<N - 1, T...> {};

template <typename T1, typename T2> struct is_same      : false_type {};
template <typename T>               struct is_same<T,T> : true_type {};

template <typename T, typename ... Ts> struct has : false_type {};
template <typename T, typename H, typename ... R>
struct has<T,H,R...> : conditional<
	is_same<T,H>::value,
	true_type,
	has<T,R...>
> {};

template <typename ...> struct size : integral_constant<size_t,0> {};
template <typename H, typename ... T> struct size<H, T...> : 
	integral_constant<size_t, size<T...>::value + 1> {};

template <typename ... Ts>
struct size<variadic<Ts...>> : size<Ts...> {};

template <size_t I, typename ... Ts>
struct at<I, variadic<Ts...>> : at<I, Ts...> {};

template <typename T, typename ... Ts>
struct has<T, variadic<Ts...>> : has<T, Ts...> {};

template <typename T> struct is_integral : has<typename remove_cv<T>::type, integral_types> {};

template <typename T> struct is_floating_point : has<typename remove_cv<T>::type, floating_point_types> {};

}}