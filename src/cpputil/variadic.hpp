#pragma once

#include "type_traits.hpp"

namespace kernel { namespace cpputil {

template <typename ... Ts>
struct variadic {
	template <template <typename...> class Template>
	struct expand : Template<Ts...> {};
};

}}