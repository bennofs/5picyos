#pragma once

#include "type_traits.hpp"

namespace kernel { namespace cpputil {

template<class T>
typename remove_reference<T>::type&& move(T&& v)
{
    return static_cast<typename remove_reference<T>::type&&>(v);
}

}}