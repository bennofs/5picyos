template<typename T>
typename cpputil::enable_if<cpputil::is_floating_point<cpputil::remove_reference<T>>::value>::type Console::print(T value) {	
}

template<typename T>
typename cpputil::enable_if<cpputil::is_integral<cpputil::remove_reference<T>>::value>::type Console::print(T value) {
	char result[22]; // Unsigned long long has 20 digits, so 22 (+ '-' + '\0') should be enough
	char* ptr = result[21];
	*ptr-- = '\0';
	bool negative = false;
	typename cpputil::remove_cv<typename cpputil::remove_reference<T>::type>::type val = value;
	if(value < 0) { negative = true; val = -val; }
	for(; val; val /= 10) {
		if(ptr == result) return; // Should NEVER happen
		*ptr-- = (val % 10)["0123456789"];
	}
	if(negative) *ptr-- = '-';
	print(ptr);
}
