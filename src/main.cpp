// main.c -- Defines the C-code kernel entry point, calls initialisation routines.
// Made for JamesM's tutorials

#include <cstdint>
#include "console.hpp"
#include "multiboot.hpp"
 
extern "C" uint32_t magic;
extern "C" void *mbd;
 
extern "C" void kmain()
{
	kernel::FramebufferDevice dev;
	kernel::Console console(dev);
	if(magic != MULTIBOOT_BOOTLOADER_MAGIC) {
		char const error[] = "Error: Multiboot magic value not correct";
		unsigned int x = 10;
		unsigned int y = 10;
		for(char c : error) {
			dev.writeChar(c, x++, y);
			if(x > 70) { x = 10; ++y; }
		}
		return;
	}
	console.setPosition(0,2);
	console.print("Welcome to 5spicyOS's console!\n$ ");
	while(1);
}
