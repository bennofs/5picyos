#pragma once
#include <cstdint>
#include <cmath>
#include "common.hpp"
#include "cpputil/tuple.hpp"

namespace kernel {

enum class Color : uint8_t {
	Black,
	Blue,
	Green,
	Cyan,
	Red,
	Magenta,
	Brown,
	LightGrey,
	Grey,
	LightBlue,
	LightGreen,
	LightCyan,
	LightRed,
	LightMagenta,
	LightBrown,
	White
};


class FramebufferDevice {
	
	public:
		
		FramebufferDevice(uint16_t* start_index = reinterpret_cast<uint16_t*>(0x0B8000)) : m_video_memory(start_index), m_current_color(Color::White), m_back_color(Color::Black) {}
		
		void setForeground(Color c) {
			m_current_color = c;
		}
		
		void setBackground(Color c) {
			m_back_color = c;
		}
		
		void writeChar(char c, uint8_t x, uint8_t y);
		void setCursorPosition(uint8_t x, uint8_t y);
		uint16_t& operator()(uint8_t x, uint8_t y) {
			return m_video_memory[x + y * 80];
		}
	
	private:
		
		static IOPort<uint8_t> m_control_register;
		static IOPort<uint8_t> m_data_register;
		
		uint16_t* m_video_memory;
		Color m_current_color, m_back_color;
};

class Console {
	
	public:
		
		typedef cpputil::pair<uint8_t, uint8_t> limits_type;
		
		Console(FramebufferDevice& dev) : m_device(&dev), m_x(0), m_y(0), m_x_limits(0,80), m_y_limits(0, 25), m_float_precision(4) {}
		
		void print(char const* string);
		template<typename T> 
		typename cpputil::enable_if<cpputil::is_integral<cpputil::remove_reference<T>>::value>::type print(T value);
		template<typename T>
		typename cpputil::enable_if<cpputil::is_floating_point<cpputil::remove_reference<T>>::value>::type print(T value);
		
		void setPosition(uint8_t x, uint8_t y) {
			m_x = x;
			m_y = y;
		}
		
		void setPrecision(uint8_t precision) {
			m_float_precision = precision; 
		}
		
		void setColor(Color f, Color b) {
			m_device->setForeground(f);
			m_device->setBackground(b);
		}
		
		void setLimits(limits_type xlimits, limits_type ylimits) {
			m_x_limits = cpputil::move(xlimits);
			m_y_limits = cpputil::move(ylimits);
		}
	
	private:
		
		void scroll();
		
		FramebufferDevice* m_device;
		uint8_t m_x, m_y;
		limits_type m_x_limits;
		limits_type m_y_limits;
		uint8_t m_float_precision;
};

#include "console.tpp"

}